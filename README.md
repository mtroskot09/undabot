## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)


## About The Project

The project is made in Vue.js. 


### Built With

* [Vue.js](https://vuejs.org/)
* [Express.js](https://expressjs.com/)


## Getting Started

To get a local copy up and running follow these simple steps.


### Prerequisites

You should have yarn installed. 


### Installation

```sh
git clone https://mtroskot09@bitbucket.org/mtroskot09/undabot.git
```
Install NPM packages
```sh
yarn install
```
Serve the project for local development together with server.js
```sh
yarn parallel
```


## License

Distributed under the MIT License. See `LICENSE` for more information.


## Contact

Marino Troskot - [marinotroskot1992@gmail.com](marinotroskot1992@gmail.com) - email

Project Link: [https://bitbucket.org/mtroskot09/undabot/](https://bitbucket.org/mtroskot09/undabot/)