import Vue from "vue";
import Vuex from "vuex";
import axios from "axios"

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		sending: false
	},
	mutations: {
		setSending: (state, val) => {
			state.sending = val
		}
	},
	getters: {
		getSending: (state) => {
            return state.sending
        }
	},
	actions: {
		sendContactMessage({commit}, postBody) {
			const apiUrl = 'http://localhost:1000/contact';
			commit('setSending', true)

			return new Promise((resolve, reject) => {
				axios.post(apiUrl, {
					data: postBody
				}).then((response) => {
	        		if(response.data) {
	        			// Simulate slower network connection
	        			setTimeout(()=>{
							commit('setSending', false)
							resolve(response.data);
						},5000)
	        		}
	      		}).catch((error) => {
	        		commit('setSending', false)
	        		if(error.response !== undefined && error.response.data) {
	        			reject(error.response.data)
	        		} else {
	        			reject(500)
	        			console.error('Error connecting to the server')
	        		}
	      		})
            })
    	},
	},
	modules: {}
});
