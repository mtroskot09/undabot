const express	   = require("express");
const bodyParser   = require("body-parser");
const cors 		   = require("cors");
const app 		   = express();
const port 		   = 1000;
const corsOptions  = {
	origin: "http://localhost:8080"
};

app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
  
app.post("/contact", (req, res) => {
    let body  = req.body
    let email = body.data.email
    let msg   = body.data.message
    res.setHeader('Content-Type', 'application/json');

    //check mail
    if(!validEmail(email)) {
    	res.status(422).send(JSON.stringify({ message: 'Please insert a valid email', sender: 'email' }));
    	return
    }

    //check message
    if(msg.length < 30) {
    	res.status(422).send(JSON.stringify({ message: 'A message must have a minimum of 30 characters', sender: 'message' }));
    	return
    }

    //send message
    res.status(200).send(JSON.stringify({ message: 'Your message has been sent'}));
});

app.listen(port, () => {
	console.log(`Server listening at http://localhost:${port}`)
});

function validEmail(email) {
	const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}